/*
* Author: tguerena and surge919
*
* Device Handler
*/


preferences {
    section("External Access"){
        input "external_on_uri", "text", title: "External On URI", required: false
        input "external_off_uri", "text", title: "External Off URI", required: false
        input "external_toggle_mute_uri", "text", title: "External Toggle Mute URI", required: false
    }

    section("Internal Access"){
        input "internal_ip", "text", title: "Internal IP", required: false
        input "internal_port", "text", title: "Internal Port (if not 80)", required: false
        input "internal_on_path", "text", title: "Internal On Path (/blah?q=this)", required: false
        input "internal_off_path", "text", title: "Internal Off Path (/blah?q=this)", required: false
        input "internal_toggle_mute_path", "text", title: "Internal Toggle Mute Path (/blah?q=this)", required: false
    }
}




metadata {
    definition (name: "LG TV Interface", namespace: "tguerena", author: "Troy Guerena") {
        capability "Actuator"
        capability "Refresh"
        capability "TV"
        capability "Music Player"
        capability "Switch"
        command "mute"
        command "on"
        command "off"
    }

    // simulator metadata
    simulator {
    }

    // UI tile definitions
    tiles {
        standardTile("power", "device.status", width: 3, height: 2, decoration: flat) {
            state "off", label: 'Off', action: "switch.on", icon: "st.switches.switch.off", backgroundColor: "#ffffff", nextState: "on"
                state "on", label: 'On', action: "switch.off", icon: "st.switches.switch.on", backgroundColor: "#79b821", nextState: "off"
        }
        standardTile("muteButton", "device.mute", canChangeIcon: true) {
            state "default", label:"Mute", icon:"https://raw.githubusercontent.com/samlalor/LGSmartTV2012/Icons/mute.png", action:"Music Player.mute"
        }
        main (["power","muteButton"])
            details (["power","muteButton"])
    }
}

def parse(String description) {
    log.debug(description)
}

def on() {
    if (external_on_uri){
        // sendEvent(name: "switch", value: "on")
        // log.debug "Executing ON"

        def cmd = "${settings.external_on_uri}";

        log.debug "Sending request cmd[${cmd}]"

            httpGet(cmd) {resp ->
                if (resp.data) {
                    log.info "${resp.data}"
                }
            }
    }
    if (internal_on_path){
        def port
            if (internal_port){
                port = "${internal_port}"
            } else {
                port = 80
            }

        def result = new physicalgraph.device.HubAction(
                method: "GET",
                path: "${internal_on_path}",
                headers: [
                HOST: "${internal_ip}:${port}"
                ]
                )
            sendHubCommand(result)
            sendEvent(name: "switch", value: "on")
            log.debug "Executing ON"
            log.debug result
    }
}

def off() {
    if (external_off_uri){
        def cmd = "${settings.external_off_uri}";
        log.debug "Sending request cmd[${cmd}]"
            httpGet(cmd) {resp ->
                if (resp.data) {
                    log.info "${resp.data}"
                }
            }
    }
    if (internal_off_path){
        def port
            if (internal_port){
                port = "${internal_port}"
            } else {
                port = 80
            }

        def result = new physicalgraph.device.HubAction(
                method: "GET",
                path: "${internal_off_path}",
                headers: [
                HOST: "${internal_ip}:${port}"
                ]
                )

            sendHubCommand(result)
            sendEvent(name: "switch", value: "off")
            log.debug "Executing OFF"
            log.debug result
    }
}

def mute()
{
    if (external_off_uri){
        def cmd = "${settings.external_toggle_mute_uri}";
        log.debug "Sending request cmd[${cmd}]"
            httpGet(cmd) {resp ->
                if (resp.data) {
                    log.info "${resp.data}"
                }
            }
    }
    if (internal_off_path){
        def port
            if (internal_port){
                port = "${internal_port}"
            } else {
                port = 80
            }

        def result = new physicalgraph.device.HubAction(
                method: "GET",
                path: "${internal_toggle_mute_path}",
                headers: [
                HOST: "${internal_ip}:${port}"
                ]
                )

            sendHubCommand(result)
            sendEvent(name: "TV", value: "mute")
            log.debug "Executing toggle mute"
            log.debug result
    }
}