var router = require('express').Router();
var wol = require('node-wol');

const ipAddress = '192.168.0.5';
var resultOk = { result: 'OK' };
var isConnected = false;
const macAddress = 'A0:6F:AA:7A:8D:F2';
var lgtv2;

function connect(callback){
  console.log('DEBUG connecting to LG TV...');
  lgtv2 = require("lgtv2")({
    url: 'ws://' + ipAddress + ':3000'
  });

  lgtv2.on('connect', function () {
    console.log('DEBUG Connected to LG TV');
    isConnected = true;
    callback ? callback() : null;
  });

  lgtv2.on('error', function (error) {
    console.log('ERROR connection error:',error);
    isConnected = false;
    callback ? callback(true) : null;
  });

  lgtv2.on('close', function () {
    console.log('DEBUG connection closed');
    isConnected = false;
    callback ? callback(true) : null;
  });
}

function executeLgTvAction(action, payload){
  return new Promise(function (resolve,reject) {
    const exec = function(shouldReject){
      if(shouldReject){
        reject('Reconnection failed');
        return;
      }

      lgtv2.request(action, payload || null ,function (err, res) {
        if(err){
          reject('executing action "'+action+'": ' + err.toString());
          lgtv2.disconnect();
        }
        console.log('DEBUG executed action:', action);
        resolve(res);
      });
    };

    isConnected ? exec() : connect(exec);
  });
}

function handleError(error,res) {
  console.error('ERROR:', error);
  res.send({error: error});
}


router.get('/tv-off', function(req, res) {
  executeLgTvAction('ssap://system/turnOff').then(function(result){
    res.send(resultOk);
  }).catch(function (error) {
    handleError(error,res);
  });
});

router.get('/tv-on', function(req, res) {
  wol.wake(macAddress, function(err) {
    if(err) {
      handleError(err,res);
      return;
    }
    res.send(resultOk);
  });
});

router.get('/tv-toggle-mute', function(req, res) {
  executeLgTvAction('ssap://audio/getStatus').then(function(result){
    const isMuted = result.mute;
    executeLgTvAction('ssap://audio/setMute', {mute: !isMuted}).then(function(result){
      res.send(resultOk);
    }).catch(function (error) {
      handleError(error,res);
    });
  }).catch(function (error) {
    handleError(error,res);
  });
});

router.get('/tv-disconnect', function(req, res) {
  lgtv2.disconnect();
  console.log('DEBUG LG TV disconnection triggered manually.');
  res.send(resultOk);
});

connect();

module.exports = router;
